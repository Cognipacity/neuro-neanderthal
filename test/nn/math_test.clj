(ns nn.math-test
  (:require
		[clojure.test :refer :all]
		[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
		[uncomplicate.neanderthal.real :refer :all]
		[uncomplicate.neanderthal.native :refer [dge dv]]
		[uncomplicate.neanderthal.math :as m #_:refer #_[exp f=]]
		[uncomplicate.neanderthal.vect-math :as vm #_:refer #_[exp! fmax! linear-frac! mul!]]
		[nn.math :refer [f= relu! relu-backward! sigmoid! sigmoid-backward!]]
		[nn.util :refer [seq-contains?]]))

(deftest sigmoid!-test
	(let [v (dv -2 -1 0 1 2)
				m (dge 2 2 [3 -2 5 4] {:layout :row})]

		(is (nil? (seq-contains?
					 	(map (partial f= 4) (sigmoid! v v) (dv [0.11920292, 0.26894142, 0.5, 0.73105858, 0.88079708]))
					 	false)))))

(deftest sigmoid-backward!-test

	;; Without dumping to double-array, an exception is thrown through the map function (due to matrix, I assume).

	(let [da	(dge 2 4 [1 2 3 4 5 6 7 8] {:layout :row})
				Z		(dge 2 4 [0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0] {:layout :row})
				dz	(sigmoid-backward! da Z)
				dza	(double-array (* 4 2))
				r		(double-array (* 4 2) [0.24613408 0.47000742 0.65368498 0.78644773 0.86552393 0.89487871 0.88290458 0.83994868])]

		(transfer! dz dza)
		;(println dz)
		;(println (map (partial f= 4) dza r))
		(is (nil? (seq-contains? (map (partial f= 4) dza r) false)))))

(deftest relu-test
	(let [z (dge 2 4 [1 -2 -3 4 0 5 -6 7] {:layout :row})
		za	(double-array (* 4 2))
		r		(double-array (* 4 2) [1 0 0 4 0 5 0 7])]

		;; Without dumping to double-array, an exception is thrown through the map function.

		(relu! z z)
		(transfer! z za)
		;(println z)

		(is (nil? (seq-contains?
								(map (partial f= 4) za r) false)))))

(deftest relu-backward!-test
	(let [da	(dge 2 4 [10 -20 -30 40 0 50 -60 70] {:layout :row})
				a		(dge 2 4 [1 -2 -3 4 0 5 -6 7] {:layout :row})
				dz	(relu-backward! da a)
				dza	(double-array (* 4 2))
				r		(double-array (* 4 2) [10 0 0 40 0 50 0 70])]

		;; Without dumping to double-array, an exception is thrown through the map function.

		;(println dz)
		(transfer! dz dza)

		(is (nil? (seq-contains? (map (partial f= 4) dza r) false)))))
