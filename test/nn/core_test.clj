(ns nn.core-test
	(:require [clojure.test :refer :all]
						[nn.core :refer :all]
						[nn.data.core :refer [canned-planar-dataset create-planar-dataset k make-DataModel pp-vm]]
						[nn.data.native :as native :refer [DataModel-factory]]
						[nn.math :refer [f=]]
						[nn.util :refer [seq-contains?]]
						[com.cognipacity.sampling.random :as sr]
					#_[fastmath.core :refer [approx-eq]]
						[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
						[uncomplicate.neanderthal.native :refer :all]
						[uncomplicate.neanderthal.real :refer :all]
						[uncomplicate.neanderthal.math :as m #_:refer #_[exp f=]]
						[uncomplicate.neanderthal.vect-math :as vm #_:refer #_[exp! linear-frac! log! tanh!]]))

(deftest neural-network-tests

	(let [model (make-NeuralNetworkModel
								(make-DataModel
									(canned-planar-dataset "6-sample canned planar dataset" "resources/dataset6.json")
									[4]
									DataModel-factory)
								:name "Configurable neural network"
								:uri "urn:com.cognipacity.ai.ml.nn"
								:learning-rate 0.1)]

		(testing "forward-propagation"

			;(pp-vm (g model :W1) "W1\n")
			;(pp-vm (g model :W2) "W2\n")

			(let [m (ncols (g model :X))]
				;W1 [0.00847726263580517 0.010945714585900015
				;	  -9.205275935008791E-4 -0.0034698699485300718
				;		0.0028927888250846684 -0.013678425526840158
				;		-0.001305643454070749 0.005304551482329159]
				;W2 [4.632782936782514E-5 0.005774916216471517 -0.002960672482528887 0.003233568418376715]
				;b1 [0.0 0.0 0.0 0.0]
				;b2 [0.0]
				;X  [1.204442292254319 0.15870990398255372 0.09524719604502052 0.3491784746396591 0.6941503784609095 1.6206503790381424
				;		3.5761141481288448 -1.4821709003683443 -1.2799553276531797 -2.064379974990181 2.8891087790998613 -4.035124003739587]
				;Y  [0.0 0.0 0.0 1.0 1.0 1.0]
				;Z1 [0.0493535 , -0.01487799, -0.01320259, -0.01963604, 0.03750786, -0.03042864,
				;		-0.01351737, 0.00499684, 0.0043536, 0.0068417, -0.01066382, 0.0125095,
				;		-0.04543141, 0.02073288, 0.0177833, 0.02924757, -0.03751043, 0.05988234,
				;		0.01739711, -0.00806947, -0.00691395, -0.01140651, 0.01441911, -0.02352051]
				;A1 [0.51233587, 0.49628057, 0.4966994, 0.49509115, 0.50937586, 0.49239343,
				;		0.49662071, 0.50124921, 0.5010884, 0.50171042, 0.49733407, 0.50312733,
				;		0.4886441, 0.50518303, 0.50444571, 0.50731137, 0.49062349, 0.51496611,
				;		0.50434917, 0.49798264, 0.49827152, 0.4971484, 0.50360472, 0.49412014]
				;Z2 [0.00307581, 0.00303224, 0.00303445, 0.00302585, 0.00307153, 0.00300146]
				;A2 [0.50076895, 0.50075806, 0.50075861, 0.50075646, 0.50076788, 0.50075036]]

				(forward-propagation model)

				;; calculate means

				(let [Z1 (g model :Z1)
							A1 (g model :A1)
							Z2 (g model :Z2)
							A2 (g model :A2)
							mz1 (/ (sum Z1) (dim Z1))
							ma1 (/ (sum A1) (dim A1))
							mz2 (/ (sum Z2) (dim Z2))
							ma2 (/ (sum A2) (dim A2))]

					;(println "Mean Z1: " mz1 "(0.0016602742236924751)")
					;(println "Mean A1: " ma1 "(0.011459388182339778)")
					;(println "Mean Z2: " mz2 "(-1.7544104727176404e-05)")
					;(println "Mean A2: " ma2 "(0.49999561397382175)")

					(are [m v]
						(f= 4 m v)
						#_(approx-eq m v 4)              ; fine, but requires 'yet another library'
						mz1 0.0016602742236924751
						ma1 0.011459388182339778
						mz2 -1.7544104727176404e-05
						ma2 0.49999561397382175))))

		(testing "compute-cost"

			(let [cost (compute-cost model)]

				(println "cost: " cost)

				(is (f= 4 cost 0.6931557039489354))))

		(testing "backward-propagation"

			;(println (-> model :data-model :cache keys sort))

			(let [dA0 (g model :dA0)
						dA1 (g model :dA1)
						dA2 (g model :dA2)  ; a.k.a. dAL
						dW1 (g model :dW1)
						dW2 (g model :dW2)
						dZ1 (g model :dZ1)
						dZ2 (g model :dZ2)
						db1 (g model :db1)
						db2 (g model :db2)]

				(backward-propagation model)

				;; / Y A2
				;; [0.0 0.0 0.0 2.00004708 1.99995164 2.00010506]
				;; / 1 - Y 1 - A2
				;; [2.00005854 1.99996747 1.99997249 0.0 0.0 0.0]

				;; dA2 (dAL) computed
				;((0 0 0.49998536470896104)
				;	(0 1 0.500008131721785)
				;	(0 2 0.500006877208284)
				;	(0 3 -1.5000353127529196)
				;	(0 4 -1.499963729050105)
				;	(0 5 -1.5000787935151556))

				;; dA2 (dAL) computed by Python
				; [2.00005854, 1.99996747, 1.99997249, -2.00004708, -1.99995164, -2.00010506]

				(let [mdA0 (/ (sum dA0) (dim dA0))
							mdA1 (/ (sum dA1) (dim dA1))
							mdA2 (/ (sum dA2) (dim dA2))
							mdW1 (/ (sum dW1) (dim dW1))
							mdW2 (/ (sum dW2) (dim dW2))
							mdZ1 (/ (sum dZ1) (dim dZ1))
							mdZ2 (/ (sum dZ2) (dim dZ2))
							mdb1 (/ (sum db1) (dim db1))
							mdb2 (/ (sum db2) (dim db2))]

					;(println "Mean mdA0: " mdA0 "(1.724565713301249e-12)")
					;(println "Mean mdA1: " mdA1 "(-6.682264373473531e-09)")
					;(println "Mean mdA2: " mdA2 "(-1.7544967216472546e-05)")
					;(println "Mean mdW1: " mdW1 "(8.845462582978379e-05)")
					;(println "Mean mdW2: " mdW2 "(-0.0009540211089627687)")
					;(println "Mean mdZ1: " mdZ1 "(-2.567293643037091e-09)")
					;(println "Mean mdZ2: " mdZ2 "(-4.3860261782676284e-06)")
					;(println "Mean mdb1: " mdb1 "(-2.5672936430128104e-09)")
					;(println "Mean mdb2: " mdb2 "(-4.3860261782676284e-06)")

					(are [m v]
						(f= 4 m v)
						mdA0 1.724565713301249e-12
						mdA1 -6.682264373473531e-09
						mdA2 -1.7544967216472546e-05
						mdW1 8.845462582978379e-05
						mdW2 -0.0009540211089627687
						mdZ1 -2.567293643037091e-09
						mdZ2 -4.3860261782676284e-06
						mdb1 -2.5672936430128104e-09
						mdb2 -4.3860261782676284e-06))))

		(testing "update-parameters"

			;; For learning_rate equal to 0.1, the Python updated W1, b1, W2, b2 mean values are:
			;; 0.001021885913189166 2.56729364301281e-10 0.0016189371063180693 4.3860261782676285e-07

			(let [nl (-> model :data-model :layer-sizes count)
						r [[0.001021885913189166 2.56729364301281e-10] [0.0016189371063180693 4.3860261782676285e-07]]]

				(loop [ln 1                        ; iterate from 1 to (nl - 1)
							 W (g model (k "W" ln))
							 b (g model (k "b" ln))
							 dW (g model (k "dW" ln))
							 db (g model (k "db" ln))]

					(when (< ln nl)                  ;we are not counting the input layer

						(axpy! (- (:learning-rate model)) dW W)
						(axpy! (- (:learning-rate model)) db b)

						;(println "mean W : " (/ (sum W) (dim W)))
						;(println "mean b : " (/ (sum b) (dim b)))

						(are [m v]
							(f= 4 m v)
							(/ (sum W) (dim W)) (first (nth r (dec ln)))
							(/ (sum b) (dim b)) (second (nth r (dec ln))))

						(recur
							(inc ln)
							(g model (k "W" (inc ln)))
							(g model (k "b" (inc ln)))
							(g model (k "dW" (inc ln)))
							(g model (k "db" (inc ln))))))))

		(testing "train"

			;(if false
				(let [model (make-NeuralNetworkModel
											(make-DataModel
												(canned-planar-dataset "400-sample canned planar dataset" "resources/dataset.json")
												[4]
												DataModel-factory)
											:name "Configurable neural network"
											:uri "urn:com.cognipacity.ai.ml.nn"
											:learning-rate 0.01)]

					(train model 10000 1000))
			)

		(testing "predict")

		(testing "accuracy")))

(deftest linear-backward-test
	(let [dZ			(dge 1 2 [ 1.62434536, -0.61175641] {:layout :row})
				Aprev		(dge 3 2 [-0.52817175, -1.07296862, 0.86540763, -2.3015387, 1.74481176, -0.7612069] {:layout :row})
				W				(dge 1 3 [ 0.3190391 , -0.24937038,  1.46210794] {:layout :row})
				b				(dv -2.06014071)
				db			(dv 1)
				dW			(dge 1 3 {:layout :row})
				dAprev	(dge 3 2 {:layout :row})]

		(mm! 1 dZ (trans Aprev) 0 dW)
		(scal! (/ 1 2) dW)

		(alter! db (fn ^double [^long i ^double x] (double (sum (row dZ i)))))
		(scal! (/ 1 2) db)

		(mm! 1 (trans W) dZ 0 dAprev)

		(is (nil? (seq-contains? (map (partial f= 4) db (dv 0.506294475)) false)))

		;; For expected output see "Building your Deep Neural Network - Step by Step v8", page 14.
		(pp-vm db "db")
		(pp-vm dW "dW")
		(pp-vm dAprev "dAprev")))
