(ns nn.core
	(:require
		[nn.data.core :refer [create-planar-dataset k load-dataset make-DataModel make-Predict-DataModel pp-vm]]
		[nn.data.native :refer [DataModel-factory DataModel-Predict-factory]]
		[nn.data.broadcast :refer [mmpcv!]]
		[nn.math :refer [relu! relu-backward! sigmoid! sigmoid-backward! tanh-backward!]]
;		[clojure.test.check.generators :as gen]
		[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
		[uncomplicate.neanderthal.math :as m #_:refer #_[exp]]
		[uncomplicate.neanderthal.real :refer :all]
		[uncomplicate.neanderthal.native :refer :all]
		[uncomplicate.neanderthal.vect-math :as vm :only [div! fmax fmax! linear-frac! log! mul mul! tanh!]]))

;; TODO floats, instead of doubles, for speed ?
;; cf. https://neanderthal.uncomplicate.org/codox/uncomplicate.neanderthal.core.html#var-vctr
;; cf. blog article(s)

;; dv (etc.)
;; https://neanderthal.uncomplicate.org/codox/uncomplicate.neanderthal.core.html#var-vctr
;; If source is an integer, creates a vector of zeroes.
;; If source is a number, puts it in the resulting vector.
;; Otherwise, transfers the data from source (a sequence, vector, etc.)
;; to the resulting vector.

;; TODO If more than 1 output node, how end up with a scalar cross entropy cost ?

;(defprotocol MachineLearningModel
;	"ML model operators declaration."
;	 (assign-dataset [this dataset]
;		 "Assign dataset compatible with initialized model. A name, and URI, may be specified."))

(defprotocol NeuralNetworkProtocol
"Neural network operations. The s parameter stands for 'self' (less typing & reading than 'self' or 'this')."

	(g [s kw] "Syntactic sugar.")
	(forward-propagation [s] "Forward pass.")
	(compute-cost [s] "Compute cost function.")
	(backward-propagation [s] "Backward pass to compute gradients.")
	(update-parameters [s] "Update the W and b matrices.")
	(train [s iterations print-cost] "Train the neural network with training set data.")
	(predict [s] [s dataset-uri] "Apply trained neural network (of >= 2 layers) to target data.")
	(accuracy [s] "Return the accuracy of the trained model."))

(defn- linear-forward
"Implement linear part of forward propagation for a single layer.
s     : 'self' (this NeuralNetworkProtocol)
ln    : layer number (identifies current layer)
Aprev : activation of previous layer
Z     : linear output of current layer"

	[s ln Aprev Z]

	(let [W (g s (k "W" ln))
				b (g s (k "b" ln))
				b-ones (g s (k "b" ln "-ones"))]

		;; zero the matrix
		;; this should not be necessary for any Z matrix since they're scaled by 0 in mmpcv! call
		;;(entry! Z 0)

		;(println "linear-forward (layer: " ln ")\n")
		;(println "Aprev " Aprev "\n")
		;(println "W " W "\n")
		;(println "b " b "\n")
		;(println "b-ones " b-ones "\n")
		(mmpcv! b 1 W Aprev 0 Z :v1 b-ones)))	; Aprev(1x6) [v alpha a b beta c & {:keys [v1]}]

(defn- linear-activation-forward
"Implement forward propagation for linear->activation layer.
s                     : 'self' (this NeuralNetworkProtocol)
ln                    : layer number (identifies current layer)
Aprev                 : previous layer's activation
forward-activation-fn : activation function to be used in this layer"

	[s ln Aprev forward-activation-fn]

	(let [Z (g s (k "Z" ln))
				Z-zeros (g s (k "Z" ln "-zeros"))
				A (g s (k "A" ln))]

		;; zero the matrix
		;; TODO remove this
		;; (entry! A 0)

		;(println "linear-activation-forward (layer: " ln ")\n")
		;(println "Aprev " Aprev "\n")
		;(println (k "Z" ln "-zeros") " " (k "Z" ln "-zeros"))
		(linear-forward s ln Aprev Z)

		(forward-activation-fn Z-zeros Z A)))
		;(println (k "A" ln) " " (g s kA))
		;(pp-vm A (str ":A" ln))

(defn- linear-backward
"Implement linear part of back propagation for a single layer.
s     : 'self' (this NeuralNetworkProtocol)
ln    : layer number (identifies current layer)
dZ    : gradient of the cost wrt the linear output of current layer
Aprev : previous layer's activation matrix
<- dA_prev : gradient of the cost wrt the activation of the previous layer, A[ln-1]; same shape as A_prev
dW : gradient of the cost wrt W[ln]; same shape as W
db : gradient of the cost wrt b[ln]; same shape as b"

	[s ln dZ]

	(let [Aprev		(g s (k "A" (dec ln)))	;prev in forward prop. sense
				mi 			(/ 1.0 (ncols Aprev))
				W				(g s (k "W" ln))
				dAprev	(g s (k "dA" (dec ln)))
				dW			(g s (k "dW" ln))
				db			(g s (k "db" ln))]

		;(println "linear-backward (layer: " ln ")\n")

		(mm! 1 dZ (trans Aprev) 0 dW)
		(scal! mi dW)
		;(pp-vm dW (str "dW" ln))

		;; zero the gradient
		;;(entry! db 0)

		;;db = mi * np.sum(dZ, axis=1, keepdims=True)
		(alter! db (fn ^double [^long i ^double x] (sum (row dZ i))))
		(scal! mi db)
		;(pp-vm db (str "db" ln))

		(if (> ln 1)
			(mm! 1 (trans W) dZ 0 dAprev))))
		;(pp-vm dAprev (str "dA" (dec ln)))))

;TODO remove these lines
;L_model_backward: L =  2
;L_model_backward: current_cache =  1  out: dA[ 1 ]  dW[ 2 ]  db[ 2 ]
;L_model_backward: l =  0 out: dA[ 0 ]  dW[ 1  db[ 1 ]

(defn- linear-activation-backward
"Implement back propagation for linear->activation layer.
s                      : 'self' (this NeuralNetworkProtocol)
ln                     : layer number (identifies current layer)
dA                     : post-activation gradient for current layer ln
backward-activation-fn : activation function to be used in this layer"

	[s ln dA backward-activation-fn]

	(let [Z		(g s (k "Z" ln))
				dZ	(g s (k "dZ" ln))]

		;(println "linear-activation-backward (layer: " ln ")\n")
		;(pp-vm dA (str "dA" ln))
		;(pp-vm Z (str "Z" ln))
		;(pp-vm Aprev (str "A" (dec ln)))
		(backward-activation-fn dA Z dZ)
		;(pp-vm dZ (str "dZ" ln))
		(linear-backward s ln dZ)))

;;TODO remove this function
#_(defn- linear-activation-backward2
"Implement back propagation for linear->activation layer.
s                      : 'self' (this NeuralNetworkProtocol)
ln                     : layer number (identifies current layer)
dA                     : post-activation gradient for current layer ln
backward-activation-fn : activation function to be used in this layer"

	[s ln _ backward-activation-fn]

	(let [A			(g s (k "A" ln))
				dZ		(g s (k "dZ" ln))
				dZinc	(g s (k "dZ" (inc ln)))
				Winc	(g s (k "W" (inc ln)))]

		(backward-activation-fn dZinc Winc A dZ)
		(linear-backward s ln dZ)))

;; TODO GPU memory must be released (cf. with-release).

(declare make-NeuralNetworkModel)

;; "Neural Network data model cache and operations implementation."
(defrecord NeuralNetworkModel [name uri data-model learning-rate prediction]

	NeuralNetworkProtocol

	(g [s kw] (-> (:data-model s) :cache kw))

	(forward-propagation [s]

		(let [nl (dec (count (:layer-sizes data-model)))	;don't count the input layer
					A (g s :X)]

			(loop [ln 1 Aprev A]

				(when (< ln nl)

					;(println "forward-propagation (layer: " ln ")\n")
					;(println "Aprev\n" Aprev)
					(linear-activation-forward s ln Aprev relu!)

					(recur
						(inc ln)
						(g s (k "A" ln)))))

			(linear-activation-forward s nl (g s (k "A" (dec nl))) #(sigmoid! %2 %3))))
			;(pp-vm (g s (k "A" nl)) (str "A" nl)))))

	(compute-cost [s]

		(let [nl (dec (count (:layer-sizes data-model)))	;don't count the input layer
					mi (/ -1 (-> s :data-model :y-dims second))
					Y (g s :Y)
					A (g s (k "A" nl))
					T1 (g s :T1)
					T2 (g s :T2)
					T3 (g s :T3)
					T4 (g s :T4)
					T5 (g s :T5)]

			;;Python: cost = float(- (np.dot(Y,np.log(A2).T) + np.dot((1-Y),np.log(1-A2).T)) / m)
      ;;                        T4                       T5
			;;T1 is LOG-A2
			;;T4 is Y-MUL-LOG-A2-T
			;;T2 is ONE-SUB-Y
			;;T3 is ONE-SUB-A2
			;;T1 is LOG-ONE-SUB-A2
			;;T5 is ONE-SUB-Y-MUL-LOG-ONE-SUB-A2-T

			(vm/log! A T1)
			(mm! 1 Y (trans T1) 0 T4)
			(vm/linear-frac! -1 Y 1 T2)
			(vm/linear-frac! -1 A 1 T3)
			(vm/log! T3 T1)
			(mm! 1 T2 (trans T1) 0 T5)
			(axpy! T4 T5)
			(scal! mi T5)
			(entry T5 0 0)))

	(backward-propagation [s]

		(let [nl (dec (count (:layer-sizes data-model)))]	;don't count the input layer

			(let [AL  (g s (k "A" nl))	; A[L] = Yhat (prediction probabilities)
						dAL (g s (k "dA" nl))	; dA[L]	(matrix is at present a buffer to be computed)
						Y   (g s :Y)					; ground truth
						T1  (g s :T1)					; temp working matrix (#nodes in output layer X #samples)
						T2  (g s :T2)]				; temp working matrix (#nodes in output layer X #samples)

				;; zero the gradient
				(entry! dAL 0)

				;(println "backward-propagation (#layers: " nl ")\n")
				;; TODO keep in mind Python stmt: Y = Y.reshape(AL.shape) # "after this line, Y is the same shape as AL"
				;; derivative of cost with respect to AL:
				;; dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))

				;; compute dAL, the derivative of the cost function wrt the output layer's activation
				(vm/linear-frac! -1 Y 1 T1)		; (1 - Y)
				;(pp-vm Y "Y")
				;(pp-vm AL "AL")
				;(pp-vm T1 "(1 - Y)")
				(vm/linear-frac! -1 AL 1 T2)	; (1 - AL)
				;(pp-vm T2 "(1 - AL)")
				(vm/div! T1 T2)								; (div! (1-Y) (1 - AL))
				;(pp-vm T1 "(div! (1-Y) (1 - AL)")
				(vm/div! Y AL T2)							; (div! Y AL)
				;(pp-vm T2 "(div! (Y AL))")
				;(pp-vm dAL "dAL")
				(axpby! -1 T2 0 dAL 1 T1)			; dAL <- - (div! Y AL) + (div! (1 - Y) (1 - AL))
				;(pp-vm dAL "dAL in backward-propagation")

				;; tanh approach, in progress
				;; (let [A2 (g s :A2) dZ2 (g s :dZ2) Y (g s :Y)] (axpby! -1 Y 0 dZ2 1 A2))
				(linear-activation-backward s nl dAL sigmoid-backward!))

			(loop [ln (dec nl)						; iterate from (L-2) down to 0;
						 dA	(g s (k "dA" ln))]	; however, for convenience, initialize ln (layer number) to (L-1)
																		; and iterate down to 1
				(when (> ln 0)

					;; tanh approach, in progress
					;; (linear-activation-backward2 s ln dA tanh-backward!)
					(linear-activation-backward s ln dA relu-backward!)

					(recur
						(dec ln)
						(g s (k "dA" (dec ln))))))))

	(update-parameters [s]

		;(println "update-parameters")
		(let [nl (count (:layer-sizes data-model))]

			(loop [ln 1											; iterate from 1 to (nl - 1)
						 W (g s (k "W" ln))
						 b (g s (k "b" ln))
						 dW (g s (k "dW" ln))
						 db (g s (k "db" ln))]

				(when (< ln nl)

					(axpy! (- learning-rate) dW W)
					(axpy! (- learning-rate) db b)

					(recur
						(inc ln)
						(g s (k "W" (inc ln)))
						(g s (k "b" (inc ln)))
						(g s (k "dW" (inc ln)))
						(g s (k "db" (inc ln))))))))

	(train [s iterations print-cost]
		(let [cost (atom 1E6)]
			(loop [i 0]
				(when (< i iterations)
					(forward-propagation s)
					(reset! cost (compute-cost s))
					(if (and print-cost (= (mod i print-cost) 0))
						(println "cost: " @cost))
					(backward-propagation s)
					(update-parameters s)
					(recur (inc i))))
			@cost))

	(predict [s]

		(let [nl	(dec (count (:layer-sizes (:data-model s))))	;don't count the input layer
					m		(-> s :data-model :y-dims second)							; number samples
					AL	(g s (k "A" nl))]															; Yhat = A[L] (activation probabilities)

			(forward-propagation s)

			;; convert the final (sigmoid) activation (AL) to 0 / 1
			;; predictions, and return a lazy sequence of the values
			(reset! (:prediction s) (for [i (range m)] (if (> (entry AL 0 i) 0.5) 1.0 0.0)))
			s))

	(predict [s dataset-uri]

		(let [p-model (make-NeuralNetworkModel
										(make-Predict-DataModel
											(load-dataset
												"Cat image feature vectors and labels for testing."
												dataset-uri)
											(#(subvec %1 1 (dec (count %1))) (:layer-sizes data-model))
											DataModel-Predict-factory
											(:cache data-model))
										:name "Prediction model to exercise this trained model"
										:uri "urn:com.cognipacity.ai.ml.nn")

					nl	(dec (count (:layer-sizes (:data-model p-model))))	;don't count the input layer
					m		(-> p-model :data-model :y-dims second)							; number samples
					AL	(g p-model (k "A" nl))]															; Yhat = A[L] (activation probabilities)

					(forward-propagation p-model)

					;; convert the final (sigmoid) activation (AL) to 0 / 1
					;; predictions, and return a lazy sequence of the values
					(reset! (:prediction p-model) (for [i (range m)] (if (> (entry AL 0 i) 0.5) 1.0 0.0)))
					p-model))

	(accuracy [s]

		(if (deref (:prediction s))
			(let [Y (g s :Y) m (ncols Y)]

				(float
					(/ (reduce +
									 	 (map #(if (= %1 %2) 1 0)
											 (deref (:prediction s))
											 (into [] (row (g s :Y) 0))))
					 m)))
			0.0)))

(defn make-NeuralNetworkModel
	[datamodel & {:keys [name uri learning-rate prediction]
								:or {name "unnamed" uri "urn:nil" learning-rate 0.0075 prediction (atom nil)}}]

	(->NeuralNetworkModel name uri datamodel learning-rate prediction))

;; TODO consider good default value for learning-rate (e.g., 0.075) and algorithms to compute it
;; TODO enforce constraint that a model instance can only be trained once OR re-initialize parameters...

;;TODO allow either function or number to be passed as w-scale argument.
;;TODO everything below belongs in test namespace

;(def rg (com.cognipacity.sampling.random/create 1))
;(def w1 (map (partial * (/ 1 (uncomplicate.neanderthal.math/sqrt 12288)))
;						 (repeatedly ( * 1 7) #(com.cognipacity.sampling.random/next-gaussian! rg))))
;(take 7 w1)

;(com.cognipacity.sampling.random/next-gaussian! rg)

;(def m1 (dge 2 2 [1 2 3 4]))
;(def m2 (dge 2 2 [10 20 30 40]))
;(vm/div! m1 m2)

(defn ws-fn-gen [v] (fn [n] (/ 1.0 (m/sqrt (nth v n)))))
(def ws-fn (ws-fn-gen [12288 20 7 5]))
;(ws-fn 0)

;(train model 2200 220)
;(accuracy (predict model))
;(def p-model (predict model "resources/cats_test.json"))
;(accuracy p-model)

(def model (make-NeuralNetworkModel
						 (make-DataModel
							 (load-dataset
								 "Cat image feature vectors and labels:
								 W scaled by 0.01 vs (/ 1 (sqrt layer_[l]_dim)) does not vs does converge."
								 "resources/cats_train.json")
							 [20 7 5]
							 DataModel-factory
							 :w-scale-fn ws-fn)	; if this is omitted, W matrices are scaled by 0.001
						 :name "Four layer (hidden layers of 20, 7, and 5 nodes) neural network"
						 :uri "urn:com.cognipacity.ai.ml.nn"
						 :learning-rate 0.0075))

#_(def model (make-NeuralNetworkModel
 						 (make-DataModel
 							 (load-dataset
 								 "Cat image feature vectors and labels."
 								 "resources/cats_train.json")
 							 [7]
 							 DataModel-factory)
 						 :name "Two layer (hidden layer of 7 nodes) neural network"
 						 :uri "urn:com.cognipacity.ai.ml.nn"))

#_(def model (make-NeuralNetworkModel
							 (make-DataModel
								 (load-dataset
									 "6-sample canned planar dataset"
									 "resources/dataset6.json")
								 [4]
								 DataModel-factory)
							 :name "Configurable neural network"
							 :uri "urn:com.cognipacity.ai.ml.nn"
							 :learning-rate 0.01))

#_(def model (make-NeuralNetworkModel
							 (make-DataModel
								 (create-planar-dataset
									 "Generated planar dataset"
									 "urn:com.cognipacity")
								 [4]
								 DataModel-factory)
							 :name "Configurable Neural Network"
							 :uri "urn:com.cognipacity.ai.ml.nn"))
