(ns nn.data.broadcast
	(:require
		[uncomplicate.commons.core :refer [with-release]]
		[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
		[uncomplicate.neanderthal.real :refer :all]))

;rk!
;(rk! alpha x y a)
;(rk! x y a)
;
;Rank-1 update.
; Multiplies vector x with transposed vector y,
; scales the resulting matrix by scalar alpha,
; and adds it to the matrix a.
;
;The contents of a will be changed.

(defn xpcv!
	"Addition of broadcast column vector v and matrix x,
	where the dimension of v equals the number of rows of x.
	Matrix x is altered.
	1s vector, y, may be pre-allocated and supplied,
	otherwise it's allocated in this routine.
	The dimension of y equals the number of columns of x."

	([x v]
	 (with-release [y (entry! (raw (row x 0)) 1)]
								 ; with-release is '(let) that ensures GPU computing deallocations'
								 (rk! v y x)))

	([x v y] (rk! v y x)))

(defn xprv!
	"Addition of broadcast row vector v and matrix x,
	where the dimension of v equals the number of columns of x.
	Matrix x is altered.
	1s vector, y, may be pre-allocated and supplied,
	otherwise it's allocated in this routine.
	The dimension of y equals the number of rows of x."

	([x v]
	 (with-release [y (entry! (raw (col x 0)) 1)]
								 ; with-release is '(let) that ensures GPU computing deallocations'
								 (rk! y v x)))

	([x v y] (rk! y v x)))

(defn mmpcv!
	"Matrix multiplication (mm!), followed by addition of a column vector broadcast over result matrix."
	[v alpha a b beta c & {:keys [v1]}]
	;(println "mmpcv\n")
	;(println "v " v "\n")
	;(println "alpha " alpha "\n")
	;(println "beta " beta "\n")
	;(println "a " a "\n")
	;(println "b " b "\n")

	(mm! alpha a b beta c)
	;(println "c " c "\n")
	(if v1 (xpcv! c v v1) (xpcv! c v)))
	;(println "c " c "\n"))

(defn mmprv!
	"Matrix multiplication (mm!), followed by addition of a row vector broadcast over result matrix."
	[v alpha a b beta c & {:keys [v1]}]
	(mm! alpha a b beta c)
		(if v1 (xprv! c v v1) (xprv! c v)))

;; TODO solve the problem of also passing (conveniently) a ones-vector

#_(defn mmpcv!
	"Matrix multiplication (mm!), followed by addition of a column vector broadcast over result matrix."
	([v alpha a b beta c]
	 (mm! alpha a b beta c) (xpcv! c v))
	([v alpha a b c]
	 (mm! alpha a b c) (xpcv! c v))
	([v alpha a b]
	 (mm! alpha a b) (xpcv! b v))
	([v a b]
	 (mm! a b) (xpcv! b v)))

#_(defn mmprv!
	"Matrix multiplication (mm!), followed by addition of a row vector broadcast over result matrix."
	([v alpha a b beta c]
	 (mm! alpha a b beta c) (xprv! c v))
	([v alpha a b c]
	 (mm! alpha a b c) (xprv! c v))
	([v alpha a b]
	 (mm! alpha a b) (xprv! b v))
	([v a b]
	 (mm! a b) (xprv! b v)))

;(defmacro mm-pbcv! [v & args] `(do (mm! 1 ~@args) (pbcv! (last [~@args]) ~v)))
;(defmacro mm-pbrv! [v & args] `(do (mm! 1 ~@args) (pbrv! (last [~@args]) ~v)))
;(defmacro mmpbv! [v bcast-fn & args] `(do (mm! 1 ~@args) (#(~bcast-fn %1 %2) (last [~@args]) ~v)))
;(macroexpand '(mmpbv! v q x r))
;(mmpbv! {:v v :a q :b x :c r})
;(mmpbv! v :row 1 q x r)
;(mmpbv! v :row 1 q x r)
;(mmpbv! z :row 1 x q s)
;(mm! 1 q x 500 r)
;(mm! 1 x q 0 s)
;(entry! r 0)
;(entry! r 1)
