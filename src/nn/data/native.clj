(ns nn.data.native
	(:require
		[com.cognipacity.sampling.random :as sr]
		[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
		[uncomplicate.neanderthal.native :refer :all]
		[uncomplicate.neanderthal.real :refer :all]
		[nn.data.core :refer [->DataModel map->DataModel k]]))

(defn DataModel-factory

	"this-fn           : this function itself
	dataset            : DataSet instance, another of same dimensions may be reassigned later.
	hidden-layer-sizes : number of nodes in each network hidden layer
	seed               : gaussian random number generator seed value (default value 1)
	w-scale-fn         : W matrices initialization scale factor function (default value #(identity 0.001))"

	[this-fn dataset hidden-layer-sizes seed w-scale-fn]

	(let [rg (sr/create seed)
				x-dims (if (number? (-> dataset :data :X first))
								 [1 (-> dataset :data :X count)]
								 [(-> dataset :data :X count) (-> dataset :data :X first count)])
				y-dims (if (number? (-> dataset :data :Y first))
								 [1 (-> dataset :data :Y count)]
								 [(-> dataset :data :Y count) (-> dataset :data :Y first count)])
				X (dge (first x-dims) (second x-dims) (into [] (-> dataset :data :X flatten)) {:layout :row})
				Y (dge (first y-dims) (second y-dims) (into [] (-> dataset :data :Y flatten)) {:layout :row})
				m (second y-dims)
				layer-sizes (into [] (flatten [(first x-dims) hidden-layer-sizes (first y-dims)]))
				nl (count layer-sizes)
				vm (for [layer (range 1 nl)
								 :let [ldc (nth layer-sizes layer)					; current layer dim
											 ldp (nth layer-sizes (dec layer))]]	; previous layer dim
						 {
							(k "Z" layer)
							(dge ldc m {:layout :row})
							(k "Z" layer "-zeros")							; pre-allocate 0s matrices for ReLU activation function
							(dge ldc m {:layout :row})
							(k "dZ" layer)
							(dge ldc m {:layout :row})
							(k "A" layer)
							(dge ldc m {:layout :row})
							(k "dA" layer)
							(dge ldc m {:layout :row})
							(k "W" layer)
							(dge ldc ldp (map (partial * (w-scale-fn (dec layer))) (repeatedly (* ldc ldp) #(sr/next-gaussian! rg))) {:layout :row})
							(k "dW" layer)
							(dge ldc ldp {:layout :row})
							(k "b" layer)
							(dv ldc)
							(k "b" layer "-ones") 							; pre-allocate 1s vectors for broadcasting b vectors
							(entry! (dv m) 1)
							(k "db" layer)
							(dv ldc)
							})
				nlast (last layer-sizes)
				yr 		(first y-dims)
				cache (assoc (into {} vm)									; realize the lazy seq
								:X	X
								:A0	X															; synonym for X, used during back prop
								:dA0 (zero X)											; used during back prop
								:Y	Y
								:T1	(dge nlast m {:layout :row})	; auxiliary matrices
								:T2	(dge nlast m {:layout :row})
								:T3	(dge nlast m {:layout :row})
								:T4	(dge yr nlast {:layout :row})
								:T5	(dge yr nlast {:layout :row}))]

		(->DataModel x-dims y-dims layer-sizes seed rg w-scale-fn cache (:name dataset) (:uri dataset) this-fn)))

(defn DataModel-Predict-factory

"this-fn           : this function itself
dataset            : DataSet instance, another of same dimensions may be reassigned later.
hidden-layer-sizes : number of nodes in each network hidden layer
parameter-map      : W, b, and b-ones matrices for each layer 1 .. L-1"

	[this-fn dataset hidden-layer-sizes parameter-map]

	(let [x-dims (if (number? (-> dataset :data :X first))
								 [1 (-> dataset :data :X count)]
								 [(-> dataset :data :X count) (-> dataset :data :X first count)])
				y-dims (if (number? (-> dataset :data :Y first))
								 [1 (-> dataset :data :Y count)]
								 [(-> dataset :data :Y count) (-> dataset :data :Y first count)])
				X (dge (first x-dims) (second x-dims) (into [] (-> dataset :data :X flatten)) {:layout :row})
				Y (dge (first y-dims) (second y-dims) (into [] (-> dataset :data :Y flatten)) {:layout :row})
				m (second y-dims)
				layer-sizes (into [] (flatten [(first x-dims) hidden-layer-sizes (first y-dims)]))
				nl (count layer-sizes)
				vm (for [layer (range 1 nl)
								 :let [ldc (nth layer-sizes layer)					; current layer dim
											 ldp (nth layer-sizes (dec layer))]]	; previous layer dim
						 {
							(k "Z" layer)
							(dge ldc m {:layout :row})
							(k "Z" layer "-zeros")			; pre-allocate 0s matrices for ReLU activation function
							(dge ldc m {:layout :row})
							(k "A" layer)
							(dge ldc m {:layout :row})
							(k "b" layer "-ones") 			; pre-allocate 1s vectors for broadcasting b vectors
							(entry! (dv m) 1)
							(k "W" layer)
							((k "W" layer) parameter-map)
							(k "b" layer)
							((k "b" layer) parameter-map)
						 })
				nlast (last layer-sizes)
				yr (first y-dims)
				cache (assoc (into {} vm)			; realize the lazy seq
								:X	X
								:A0	X									; synonym for X, used during back prop
								:Y	Y)]

		(map->DataModel {:x-dims x-dims
										 :y-dims y-dims
										 :layer-sizes layer-sizes
										 :cache cache
										 :dataset-name (:name dataset)
										 :dataset-uri (:uri dataset)
										 :factory this-fn})))
