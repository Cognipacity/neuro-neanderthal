(ns nn.data.core
	(:require
		[cheshire.core :as json :refer [parse-string]]
		[com.cognipacity.sampling.random :as sr :only [create next-gaussian!]]
		[uncomplicate.neanderthal.core :refer [dim mrows ncols vctr?]]
		[uncomplicate.neanderthal.real :refer [entry]]
		[nn.math :refer [linspace]]))

;; TODO find out why can't use (find) static Java method as mapped function
(defn- sin [x] (Math/sin x))
(defn- cos [x] (Math/cos x))

#_(def ^:private rg (sr/create 1))

(defrecord DataSet [name uri data])

(defn create-planar-dataset
	"num-samples  : num-samples (400)
	max-ray	     : maximum number of rays of flower = 4
	seed         : gaussian random number generator seed value (optional, default value 1)
	<-- dataset  : instance of DataSet"

	[name uri & {:keys [num-samples max-ray seed] :or {num-samples 400 max-ray 4 seed 1}}]

	{ :pre [(> num-samples 1)
					(even? num-samples)
					(int? num-samples)
					(> max-ray 0)
					(int? max-ray)]}

	(let [rng (sr/create seed)
				d 2
				m num-samples
				n (/ m d)
				Y (vec (into (repeat n 1) (repeat n 0)))
				X	; X is a LazySeq of 2 LazySeq
				(for [j (range 2)
							:let [theta (map +
															 (linspace (* j 3.12), (* (inc j) 3.12), n)
															 (map (partial * 0.2) (repeatedly n #(sr/next-gaussian! rng))))
										stheta (map sin theta)
										ctheta (map cos theta)
										theta4 (map (partial * 4) theta)
										stheta4 (map sin theta4)
										r1 (map (partial * max-ray) stheta4)
										r2 (map (partial * 0.2) (repeatedly n #(sr/next-gaussian! rng)))
										r	(map + r1 r2)
										rs (vec (map * r stheta))
										rc (vec (map * r ctheta))
										r (into rs rc)]]
					r)]

		(->DataSet name uri {:X X, :Y Y})))

(defn make-DataSet [name uri X Y & {:keys [name uri] :or {name "unnamed" uri "urn:nil"}}]
	(->DataSet name uri {:X X, :Y Y}))

(defn load-json
	"Given a filename, reads a JSON file and returns it, parsed, with keywords."
	[file]
	(json/parse-string (slurp file) true))

(defn load-dataset
	"Read features and labels dataset from JSON file."
	;; e.g. file URI syntax, for resources/dataset6.json
	;; file:resources/dataset6.json
	;; file:///Volumes/---path---/nn/resources/dataset6.json
	;; file:/Volumes/---path---/nn/resources/dataset6.json
	[name uri]
	(let [dataset (load-json uri)
				X ((:data dataset) :X)
				Y ((:data dataset) :Y)]

		(->DataSet name uri {:X X, :Y Y})))

(defrecord DataModel [x-dims y-dims layer-sizes seed rg w-scale-fn cache dataset-name dataset-uri factory])

;(number? (-> (canned-planar-dataset "foo" "file:resources/dataset6.json") :data :Y first))
;(-> (canned-planar-dataset "foo" "file:resources/dataset6.json") :data :Y count)

(defn make-DataModel

"dataset           : DataSet instance, another of same dimensions may be reassigned later.
num-hidden-layers : number of nodes in each network layer [input, hidden, .., output]
factory           : factory that creates native, CUDA, or OpenCL implementation
seed              : gaussian random number generator seed value (optional, default value 1)
w-scale-fn        : W matrices initialization scale factor function (default value #(identity 0.0075))"

	[dataset hidden-layer-sizes factory & {:keys [seed w-scale-fn] :or {seed 1 w-scale-fn (fn [_] 0.0075)}}]

	(factory factory dataset hidden-layer-sizes seed w-scale-fn))

(defn make-Predict-DataModel

"dataset           : DataSet instance, another of same dimensions may be reassigned later.
num-hidden-layers : number of nodes in each network layer [input, hidden, .., output]
factory           : factory that creates native, CUDA, or OpenCL implementation
parameter-map     : sub-map (:W, :b, :b-ones) of the trained NeuralNetworkModel's :cache map"

	[dataset hidden-layer-sizes factory parameter-map]

	(factory factory dataset hidden-layer-sizes parameter-map))

(defn pp-vm
	"Pretty-print Neanderthal vector or matrix."
	([vm]
	 (let [result
				 (if (vctr? vm)
					 (for [i (range (dim vm))
								 :let [e (entry vm i)]] `(~i ~e))
					 (for [i (range (mrows vm)) j (range (ncols vm))
								 :let [e (entry vm i j)]] `(~i ~j ~e)))]
		 (clojure.pprint/pprint result)))
	([vm name] (pp-vm vm) (print (str name)) (println "\n")))

(defn k [name & rest] (keyword (str name (clojure.string/join rest))))
