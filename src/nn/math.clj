(ns nn.math
	(:require
		[uncomplicate.neanderthal.core :refer :all :exclude [amax asum dot entry entry! ls-residual nrm2 sum]]
		[uncomplicate.neanderthal.real :refer :all]
		[uncomplicate.neanderthal.native :refer [dge]]
		[uncomplicate.neanderthal.math :as m #_:refer #_[exp]]
		[uncomplicate.neanderthal.vect-math :as vm #_:refer #_[exp! fmax! linear-frac! mul!]]))

(defn f=
	"Proximate equality for double precision floats.
	tolerance : ~ number of digits of precision (default 16);
	Defined tolerance is actually implemented in terms of a multiplier of
	EPSILON (1.1102230246251565E-16), where the default multiplier is 1."
	([x y] (m/f= x y))
	([tolerance x y] (m/f= x y (m/pow 10 (- 16 tolerance)))))

(defn sigmoid
	"Compute the sigmoid of x.
	x is a scalar
	Returns sigmoid(x)"
	[x]
	(/ 1 (+ 1 (m/exp (* -1 x)))))

;; Any routines that are computed on the CPU, as opposed to an NVIDIA or OpenCL GPU
;; should probably end up in a 'native' sub-package.

;; Vectorized sigmoid function computation:
;; Multiply 1 / (1 + exp(-x) by exp(x) --> exp(x) / (1 + exp(x))
;;
;; https://software.intel.com/en-us/mkl-developer-reference-c-v-linearfrac#CE238AF7-9607-4E7C-9EF2-7C9920EF664A
;;
;; The Intel MKL v?LinearFrac function performs a linear fraction transformation
;; of vector a by vector b with scalar parameters, i.e.,
;; scaling multipliers scalea, scaleb
;; shifting addends shifta, shiftb...
;;
;;   y[i] = (scalea·a[i]+shifta)/(scaleb·b[i]+shiftb), i=1,2 … n
;;
;; Neanderthal : (linear-frac! scalea a shifta scaleb b shiftb y) returns y
;;
;; sigmoid! : first, exp(in) -> out; return out[i] / (out[i] + 1)

(defn sigmoid!
	"Vectorized sigmoid function computation:
	Multiply 1 / (1 + exp(-x) by exp(x) -> exp(x) / (1 + exp(x)).
	Neanderthal's (linear-frac! scalea a shifta scaleb b shiftb y) returns y.
	Here, 'a', 'b', and 'y' are 'out'. out <- exp(in) then return out[i] / (out[i] + 1)."

	[in out]

	(vm/exp! in out)

	(vm/linear-frac! 1.0 out 0.0 1.0 out 1.0 out) ; out := exp(x) / (exp(x) + 1)
	;(nn.data.core/pp-vm in  "sigmoid!: input matrix")
	;(nn.data.core/pp-vm out "sigmoid!: output matrix")
	out)

(defn sigmoid-backward!
"Vectorized implementation of backward propagation for a single Sigmoid unit.
da : (dA) post-activation gradient 'of any shape'
z : (Z) matrix for the subject layer
dz : [when arity is 3] (dZ) pre-allocated matrix to receive gradient of Z
<-- dz : (dZ) gradient of cost function w.r.t. Z"

	([da z]
	 (sigmoid-backward! da z (zero z)))

	([da z dz]
	  ;(println "sigmoid-backward!\n" "dA\n" da "Z\n" z "\n" "dZ\n" dz)
		;s = 1/(1+np.exp(-Z))
;		(sigmoid! z dz)					;dz is 's'
		;dZ = dA * s * (1-s)
	 ;;TODO tweak this to use pre-allocation (or ?)
	 (let [daxs (zero dz)
				 s (zero dz)
				 sm1 (zero dz)]

			(sigmoid! z s)
			(vm/linear-frac! -1 s 1 sm1)
			(vm/mul! da sm1 daxs)
			;(println "sigmoid-backward!\n" " : s\n" s "\n1-s"  sm1 "\ndaxs" daxs)
;			(vm/mul! da (vm/linear-frac! -1 s 1 s) daxs)
			(vm/mul! s daxs dz)	;3rd matrix is overwritten
			;(println "sigmoid-backward!\n" "dA\n" da "Z\n" z "\n" "dZ\n" dz)
		 ;(vm/mul! dz (vm/mul! da (vm/linear-frac! -1 s 1 s) daxs)))
		 ;vm/mul! two-operand order seems wrong by definition to me
	 dz)))

(defn relu!
"Vectorized ReLU activation function.
z0 is a 0s-matrix with the same dimensions as z."

	([z a] (vm/fmax! (zero z) z a) a)

	([z0 z a] #_(println "relu!\n" z0 "\n" z "\n" a) (vm/fmax! z0 z a) a))

(defn relu-backward!
"Vectorized implementation of backward propagation for a single ReLU unit.
da : (dA) post-activation gradient 'of any shape'
a : (A) matrix for the subject layer
dz : if supplied (arity 3) it must be zeroed beforehand (we pass in the layer's 'Z-zeros' matrix);
if not supplied (arity 2), it's allocated, having same dimensions as da (dA)
<-- dZ : gradient of cost function w.r.t. A, with same dimensions as A matrix"

	;;TODO may have to copy adjusted (to dz) shape da to dz ?!
	; dZ = np.array(dA, copy=True) # just converting dz to a correct object.

	([da a] (let [dz (zero da)] (relu-backward! da a dz)))

	([da a dz]
		(copy! da dz)
		(alter! dz (fn ^double [^long i ^long j ^double x] (double (if (pos? (entry a i j)) x 0.0)))) dz))
		;;TODO time these alternatives
		;; vs.
		;; (alter! dz (fn ^double [^long i ^long j ^double x] (double (if (pos? (entry a i j)) (entry da i j) 0.0))))

(defn tanh-backward!

	;;TODO tanh & tanh-backward are in progress
	;; dZ1 = np.dot(W2.T, dZ2) * (1 - np.power(A1, 2))

	([dzinc winc a] (let [dz (zero a)] (tanh-backward! dzinc winc a dz)))

	([dzinc winc a dz]

	(let [buf (zero dz) buf2 (zero dz)]

		(vm/linear-frac! -1 (vm/pow! a 2 buf) 1 buf)
		(mm! 1 (trans winc) dzinc 0 buf2)
		(vm/mul! buf buf2 dz)
		dz)))

(defn linspace

	"cf. Matlab, numpy linspace()
	start value
	stop value
	nump number of points (nump - 1 intervals)"

	[start stop nump]

	{ :pre [(> nump 1)]}

	(let [step (double (/ (- stop  start) (dec nump)))]
		(for [n (range 0 nump) :let [x (* n step)]] (+ start x))))

#_(defn linspace
		"cf. numpy.linspace()
		nump (number of points)
		endp (include endpoint)"

		[start stop & {:keys [nump endp] :or {nump 50 endp true}}]

		(let [step (/ (- stop  start) (dec nump))]
			(if (not endp)
				(linspace start (- stop step) :nump nump :endp true)
				(for [n (range 0 nump) :let [x (+ start (* n step))]] x))))

;; Note that in Python code, datatype of Y is declared as 'uint8'.
