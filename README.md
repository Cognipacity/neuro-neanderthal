# neuro-neanderthal

A Clojure library providing a configurable neural network, inspired by
the first course in Coursera's **Deep Learning Specialization**:
>Neural Networks and Deep Learning

This library will presently add neural network optimization and customization,
including those presented in the second and third courses:

>Improving Deep Neural Networks:
>Hyperparameter Tuning, Regularization and Optimization*

>Structuring Machine Learning Projects

The implementation uses **Neanderthal**:
https://neanderthal.uncomplicate.org/

## Usage

```clojure

(ns foo
  (require
     [nn.core :refer [make-NeuralNetworkModel]]
     [nn.data.core :refer [make-DataModel make-DataSet]]
     [nn.data.native :refer [DataModel-factory]]

;; native DataModel-factory means run on CPU,
;; also can run on OpenCL or CUDA GPU.

;; create a Dataset with:
;;   name (optional)
;;   uri (optional)
;;   features X [[features][samples]]
;;   labels Y [[features][samples]]

(let [X-train <features>
      Y-train <labels>
      dataset (make-DataSet X-train Y-train)
      datamodel (make-DataModel dataset DataModel-factory)
      model (make-NeuralNetworkModel datamodel)
      cost-train (train model)
      accuracy-train (accuracy model)
      X-test <features>
      Y-test <labels> ; used to auto-compute accuracy
      dataset (make-DataSet X-test Y-test)
      predictions (predict model)
      accuracy-predict (accuracy model)]

  ;; etc. )
```

## License

Copyright © 2018 Christopher Hugh Graham

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.