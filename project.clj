(defproject nn "0.1.0-SNAPSHOT"

  :description "A configurable neural network, inspired by
                Coursera Deep Learning Specialization
                Course 1 : Neural Networks and Deep Learning.
                The implementation uses Neanderthal,
                https://neanderthal.uncomplicate.org/"

  :url "http://cognipacity.com/"

  :license {:name "Eclipse Public License" :url "http://www.eclipse.org/legal/epl-v10.html"}

;	:repositories {"project" "file:repo"}

	:dependencies [[org.clojure/clojure "1.9.0"]
								 ;[org.clojure/test.check "0.9.0"]
								 ;[com.taoensso/timbre "4.10.0"]
								 ;[generateme/fastmath "1.1.0"]
								 [uncomplicate/neanderthal "0.20.4"]
								 [cheshire "5.8.0"]
								 [com.cognipacity/sampling "0.1.0-SNAPSHOT"]]

	; developing on macOS
  :exclusions [[org.jcuda/jcuda-natives :classifier "apple-x86_64"]
						   [org.jcuda/jcublas-natives :classifier "apple-x86_64"]]

  ;:jvm-opts ["--add-modules" "java.xml.bind"]

  :plugins [[lein-codox "0.10.4"]
						[lein-jupyter "0.1.16"]]

  :global-vars {*warn-on-reflection* false})
